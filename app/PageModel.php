<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class PageModel extends Model
{    

    protected $guard_name = 'web';
    protected $table = 'page';
    protected $fillable = [
        'users_id','kategori_id','judul','headerImage','url','isi'
    ];

    public function menu()
    {
        return $this->belongsToMany('App\MenuGroupingModel','page_menu','page_id','menu_id');
    }
    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function kategori()
    {
        return $this->belongsTo('App\KategoriPageModel');
    }
}
