<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriPageModel;
use Auth;
use DB;

class KategoriPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategoriPages = DB::table('kategori_page')
        ->join('users','users.id','=','kategori_page.users_id')
        ->select('kategori_page.*','users.name')
        ->get();
        return view('admin.kategoriPage.index',['kategoriPages' => $kategoriPages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.kategoriPage.create');
    }
    public function headerImage($headerImage)
    {
        //Get Filename with The Extension
        $filenameWithExt = $headerImage->getClientOriginalName();

        //Get Just Filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        
        //Get Just Ext
        $extension = $headerImage->getClientOriginalExtension();

        //Filename to Store
        $filenameToStore = $filename.'_'.time().'.'.$extension;

        $path = $headerImage->storeAs('public/kategoriPage/headerImage',$filenameToStore);
        
        return $filenameToStore;
        
    }
    public function store(Request $request)
    {
        if($request->hasFile('image'))
        {
            try {
                $kategoriPage = new KategoriPageModel;
                $kategoriPage->users_id = Auth::user()->id;
                $kategoriPage->nama = $request->input('nama');
                $kategoriPage->image = $this->headerImage($request->file('image'));
                $kategoriPage->save();

                return back()->with('success','Data Berhasil di Tambahkan');
            } catch (Exception $e) { 
                return back()->with('danger', $e->getMessage());
            }
        } else {
            try {
                $kategoriPage = new KategoriPageModel;
                $kategoriPage->users_id = Auth::user()->id;
                $kategoriPage->nama = $request->input('nama');
                $kategoriPage->image = 'default_image.jpg';
                $kategoriPage->save();

                return back()->with('success','Data Berhasil di Tambahkan');
            } catch (Exception $e) { 
                return back()->with('danger', $e->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoriPage = KategoriPageModel::findOrFail($id);
        return view('admin.kategoriPage.editKategoriPage',['kategoriPage' => $kategoriPage]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('image'))
        {
            try {
                $kategoriPage = KategoriPageModel::findOrFail($id);
                $deleteHeaderImage = public_path().'/storage/kategoriPage/headerImage/'.$artikel->headerImage;
                File::delete($deleteHeaderImage);
                $kategoriPage->users_id = Auth::user()->id;
                $kategoriPage->nama = $request->input('nama');
                $kategoriPage->image = $this->headerImage($request->file('image'));
                $kategoriPage->save();

                return back()->with('success','Data Berhasil di Tambahkan');
            } catch (Exception $e) { 
                return back()->with('danger', $e->getMessage());
            }
        } else {
            try {
                $kategoriPage = KategoriPageModel::findOrFail($id);
                $kategoriPage->users_id = Auth::user()->id;
                $kategoriPage->nama = $request->input('nama');
                $kategoriPage->image = 'default_image.jpg';
                $kategoriPage->save();

                return back()->with('success','Data Berhasil di Tambahkan');
            } catch (Exception $e) { 
                return back()->with('danger', $e->getMessage());
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kategoriPage = KategoriPageModel::findOrFail($id);
            $deleteHeaderImage = public_path().'/storage/kategoriPage/headerImage/'.$artikel->headerImage;
            File::delete($deleteHeaderImage);
            $kategoriPage->delete();                    
        } catch (Exception $e) {
            return response()->json([
                'status' => $e->getMessage(),
                'error' => true,                
            ]);
        }
    }
    public function getPagePerKategori($id)
    {
        $getPagePerKategori = KategoriPageModel::with(['page','user'])->findOrFail($id);
        // dd($getPagePerKategori);
        return view('admin.kategoriPage.getPagePerKategori',['getPagePerKategori' => $getPagePerKategori]);
    }
}
