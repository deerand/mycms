@extends('layouts.app')
@section('title')
    Daftar Kategori Page
@endsection
@section('headerPage')
    Daftar Kategori Page
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
    <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-list"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Daftar Kategori Page
                    </h3>
                </div>
                
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ url('kategoriPage/create') }}" class="btn m-btn btn-danger btn-sm m-btn--icon m-btn--pill m-btn--air">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Kategori</span>
                            </span>
                        </a>
                        <a href="{{ url('page/create') }}" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air" style="margin-left:5px;">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Page</span>
                            </span>
                        </a>
                    </li>                    
                </ul>
            </div>
        </div>    
    <div class="m-portlet__body no-pedding">
        <div class="list-section">
            @forelse($kategoriPages as $kategoriPage)
            <div class="list-section__item">
                <div class="section__content image-box">
                    <div class="section__widget">
                        <div class="section__img">
                        <img src="{{asset('storage/kategoriPage/headerImage/'.$kategoriPage->image)}}" class="imageThumb">
                        </div>
                    </div>
                    <div class="section__desc">
                        <h5 class="section__title">{{ $kategoriPage->nama }}</h5>
                        <div class="section__info">
                            <div class="section__info__item sm-text">
                                <span class="info__label">Posted By :</span>
                                <a href="" class="info__detail m-link">{{ $kategoriPage->name }}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section__action">
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--up" m-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="btn m-btn btn-outline-metal m-btn--icon btn-sm m-btn--icon-only m-btn--square  m-dropdown__toggle">
                            <i class="la la-ellipsis-h"></i>
                        </a>
                        <div class="m-dropdown__wrapper" style="z-index: 101;">
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                            <li class="m-nav__section m-nav__section--first">
                                                <span class="m-nav__section-text">Quick Actions</span>
                                            </li>
                                            <li class="m-nav__separator m-nav__separator--fit"></li>
                                            <li class="m-nav__item">
                                                <a href="{{ url('getPagePerKategori/'.$kategoriPage->id)}}" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-gear"></i>
                                                    <span class="m-nav__link-text">Manage Page</span>
                                                </a>
                                            </li>
                                            <li class="m-nav__item">
                                                <a href="#!" class="m-nav__link m_sweetalert_5">
                                                    <i class="m-nav__link-icon la la-trash"></i>
                                                    <span class="m-nav__link-text">Delete Kategori Page</span>
                                                </a>
                                            </li>
                                        </ul>                                    
                                    </div>
                                </div>
                            </div>
                        <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                        </div>
                    </div>
                </div>
            </div>
            @empty
                <p>Data Kosong</p>
            @endforelse            
        </div>
    </div>
</div>
@endsection