<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listeners\HasUploadedImageListener;
use App\PageModel;
use DB;
use Auth;
use Illuminate\Support\Str;
use App\KategoriPageModel;
use Image;
class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = PageModel::with(['kategori','users'])->get();        
        return view('admin.page.index',['page' => $page]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoris = KategoriPageModel::all();
        return view('admin.page.create',['kategoris' => $kategoris]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteImageIsi(Request $request) {        
        $src = $request->input('src');
        $getFileImage = str_replace(url('/uploadPage/'), '',$src);
        
        $deleteImageIsi = public_path().'/uploadPage/'.$getFileImage;
        File::delete($deleteImageIsi);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function summernote($isi)
    {        
        $dom = new \DomDocument();
        $dom->loadHtml($isi, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){        
            $data = $img->getAttribute('src');            
            if(preg_match('/data:image/',$data))
            {
                preg_match('/data:image\/(?<mime>.*?)\;/', $data, $groups); // pecah-pecah data
                $mimetype = $groups['mime'];
                $image_name = "/storage/photos/1/summernoteImage/" . time().$k.'.png';
                $image_name_thumbs = "/storage/photos/1/summernoteImage/thumbs" . time().$k.'.png';    
                $image = Image::make($data)
                ->encode($mimetype, 100)  // encode file to the specified mimetype
                ->save(public_path($image_name),100);
                $image = Image::make($data)
                ->encode($mimetype, 100)  // encode file to the specified mimetype
                ->save(public_path($image_name_thumbs),100);
                $new_src = asset($image_name); // convert menjadi tag img html biasa
                $new_src_thumbs = asset($image_name_thumbs); // convert menjadi tag img html biasa                
                $img->removeAttribute('src');    
                $img->setAttribute('src', $image_name);
                $img->setAttribute('src', $image_name_thumbs);
            } else {
                event(new HasUploadedImageListener());
                
            }
        }
        $detail = $dom->saveHTML();

        return $detail;
    }
    public function headerImage($headerImage)
    {
        //Get Filename with The Extension
        $filenameWithExt = $headerImage->getClientOriginalName();

        //Get Just Filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        
        //Get Just Ext
        $extension = $headerImage->getClientOriginalExtension();

        //Filename to Store
        $filenameToStore = $filename.'_'.time().'.'.$extension;

        $path = $headerImage->storeAs('public/page/headerImage',$filenameToStore);
        
        return $filenameToStore;
        
    }    
    public function store(Request $request)
    {
        if($request->file('headerImage'))
        {
        try {
            $page = new PageModel;
            $this->headerImage($request->file('headerImage'));
            $page->users_id = Auth::user()->id;
            $page->kategori_id = $request->input('kategori_id');
            $page->judul = $request->input('judul');
            $page->headerImage = $this->headerImage($request->file('headerImage'));
            $page->url = Str::slug($request->input('judul'));
            $page->isi = $this->summernote($request->input('isi'));
            $page->save();

            return back()->with('success','Data Berhasil di Tambah');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
        } else {
        try {
            $page = new PageModel;
            $page->users_id = Auth::user()->id;
            $page->kategori_id = $request->input('kategori_id');
            $page->judul = $request->input('judul');
            $page->headerImage = 'default_image.jpg';
            $page->url = Str::slug($request->input('judul'));
            $page->isi = $this->summernote($request->input('isi'));
            $page->save();

            return back()->with('success','Data Berhasil di Tambah');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = PageModel::findOrFail($id);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if($request->hasFile('headerImage'))
            {
                $page = PageModel::findOrFail($id);
                $page->users_id = Auth::user()->id;
                $page->kategori_id = $request->input('kategori_id');
                $page->judul = $request->input('judul');
                $page->headerImage = $this->headerImage($request->file('headerImage'));
                $page->url = Str::slug($request->input('judul'));
                $page->isi = $this->summernote($request->input('isi'));
                $page->save();
            } else {
                $page = PageModel::findOrFail($id);
                $page->users_id = Auth::user()->id;
                $page->kategori_id = $request->input('kategori_id');
                $page->judul = $request->input('judul');
                $page->headerImage = 'default_image.jpg';
                $page->url = Str::slug($request->input('judul'));
                $page->isi = $this->summernote($request->input('isi'));
                $page->save();
            }
        } catch (\Exception $th) {
            return response()->json([
                'status' => $e->getMessage(),
                'error' => true,                
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = PageModel::findOrFail($id);
        $page->delete();
    }
}
