<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArtikelModel;
use App\KategoriArtikelModel;
use App\SectionArtikelModel;
use App\Http\Requests\ArtikelRequest;
use Auth;
use DB;
use DataTables;
use File;
use Illuminate\Support\Str;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $artikel;
    public function __construct(ArtikelModel $artikel) {
        $this->artikel = $artikel;
    }

    public function index() {
        return view('admin.artikel.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $kategoris = KategoriArtikelModel::all();
        $sections = SectionArtikelModel::all();
        return view('admin.artikel.create',[
            'kategoris' => $kategoris,
            'sections' => $sections
        ]);
    }

    public function deleteImageIsi(Request $request) {        
        $src = $request->input('src');
        $getFileImage = str_replace(url('/upload/'), '',$src);
        
        $deleteImageIsi = public_path().'/upload/'.$getFileImage;
        File::delete($deleteImageIsi);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function summernote($isi)
    {
        $dom = new \DomDocument();
        $dom->loadHtml($isi, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= "/upload/" . time().$k.'.png';    
            $path = public_path() . $image_name;    
            file_put_contents($path, $data);    
            $img->removeAttribute('src');    
            $img->setAttribute('src', $image_name);
        }
        $detail = $dom->saveHTML();

        return $detail;
    }
    public function headerImage($headerImage)
    {
        //Get Filename with The Extension
        $filenameWithExt = $headerImage->getClientOriginalName();

        //Get Just Filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        
        //Get Just Ext
        $extension = $headerImage->getClientOriginalExtension();

        //Filename to Store
        $filenameToStore = $filename.'_'.time().'.'.$extension;

        $path = $headerImage->storeAs('public/artikel/headerImage',$filenameToStore);
        
        return $filenameToStore;
        
    }
    public function store(ArtikelRequest $request)
    {
        if($request->hasFile('headerImage'))
        {
            try {
                $isi = $request->input('isi');
                $this->summernote($isi);
                $headerImage = $request->file('headerImage');
                $this->headerImage($headerImage);
                $this->artikel->create([
                    'kategori_id' => $request->input('kategori_id'),
                    'section_id' => $request->input('section_id'),
                    'users_id' => Auth::user()->id,
                    'judul' => $request->input('judul'),
                    'url' => Str::slug($request->input('judul'),'-'),
                    'headerImage' => $this->headerImage($request->file('headerImage')),
                    'isi' => $this->summernote($isi),
                    'status_artikel' => $request->input('status_artikel'),
                ]);        
                return back()->with('success', 'Data Berhasil di Masukan');
            } catch (\Exception $e) {
                return back()->with('danger', $e->getMessage());
            }            
        } else {
            try {
                $isi = $request->input('isi');
                $this->summernote($isi);
                $headerImage = $request->file('headerImage');
                $this->headerImage($headerImage);
                $this->artikel->create([
                    'kategori_id' => $request->input('kategori_id'),
                    'section_id' => $request->input('section_id'),
                    'users_id' => Auth::user()->id,
                    'judul' => $request->input('judul'),
                    'url' => Str::slug($request->input('judul'),'-'),
                    'headerImage' => 'default_image.jpg',
                    'isi' => $this->summernote($isi),
                    'status_artikel' => $request->input('status_artikel'),
                ]);        
                return back()->with('success', 'Data Berhasil di Masukan');
            } catch (\Exception $e) {
                return back()->with('danger', $e->getMessage());
            }            

        }
    }
    public function show($id)
    {
        $artikel =  DB::table('artikel')
                    ->join('kategori_artikel', 'kategori_artikel.id', '=','artikel.kategori_id')
                    ->join('users', 'users.id', '=', 'artikel.users_id')
                    ->select('artikel.id', 'artikel.headerImage', 'artikel.judul', 'artikel.isi', 'artikel.created_at', 'kategori_artikel.nama', 'users.name', 'users.email')
                    ->where('artikel.id','=',$id)
                    ->get();        
        return view('admin.artikel.viewArtikel',['artikel' => $artikel[0]]);
    }

    public function getTableArtikel()
    {
        $getArtikel = DB::table('kategori_artikel')->join('artikel','kategori_artikel.id','=','artikel.kategori_id')->select('artikel.id','kategori_artikel.nama','artikel.judul','artikel.status_artikel')->get();

        return Datatables::of($getArtikel)
            ->addColumn('action',function($artikel){
                return view('admin.artikel.buttonRole',['artikel' => $artikel])->render();
            })
            ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artikel = DB::table('artikel')
        ->join('kategori_artikel','artikel.id','=','kategori_artikel.kategori_id')
        ->join('section_artikel','artikel.id','=','section_artikel.section_id')
        ->select('artikel.*','kategori_artikel.id as kategori_id','section_artikel.id as section_id')
        ->where('artikel.id','=',$id)->get();
        $kategori = KategoriArtikelModel::all();
        $section = SectionArtikelModel::all();
        return response()->json([
            'artikel_form' => $artikel,
            'kategori' => $kategori,
            'section' => $section
        ], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->hasFile('headerImage'))
        {
        try {
            $artikel = ArtikelModel::findOrFail($id);
            $isi = $request->input('isi');
            $this->summernote($isi);
            $headerImage = $request->file('headerImage');
            $this->headerImage($headerImage);                
            $artikel->kategori_id = $request->input('kategori_id');
            $artikel->users_id = Auth::user()->id;
            $artikel->judul = $request->input('judul');
            $artikel->url = Str::slug($request->input('judul'),'-');
            $artikel->headerImage = $this->headerImage($headerImage);
            $artikel->isi = $this->summernote($isi);
            $artikel->status_artikel = $request->input('status_artikel');
            $artikel->save();                
        } catch (\Exception $e) {
            return response()->json([
                'status' => $e->getMessage(),
                'error' => true,                
            ]);
        }
        } else {
            $artikel = ArtikelModel::findOrFail($id);
            $isi = $request->input('isi');
            $this->summernote($isi);
            $deleteHeaderImage = public_path().'/storage/artikel/headerImage/'.$artikel->headerImage;
            File::delete($deleteHeaderImage);    
            $artikel->kategori_id = $request->input('kategori_id');
            $artikel->users_id = Auth::user()->id;
            $artikel->judul = $request->input('judul');
            $artikel->url = Str::slug($request->input('judul'),'-');
            $artikel->headerImage = 'default_image.jpg';
            $artikel->isi = $this->summernote($isi);
            $artikel->status_artikel = $request->input('status_artikel');
            $artikel->save();    
            return response()->json([
                'status' => 'Sukses',
                'error' => false,
                'data' => $artikel
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $artikel = ArtikelModel::findOrFail($id);
            $deleteHeaderImage = public_path().'/storage/artikel/headerImage/'.$artikel->headerImage;
            File::delete($deleteHeaderImage);
            $artikel->delete();
            return response()->json([
                'status' => 'Sukses',
                'error' => false,
                'data' => $artikel
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => $e->getMessage(),
                'error' => true,                
            ]);
        }
    }
    public function gantiStatus($id)
    {
        try {
            $artikel = ArtikelModel::findOrFail($id);
            $artikel->status_artikel = !$artikel->status_artikel;
            $artikel->save();
            return response()->json([
                'status' => 'Sukses',
                'error' => false,
                'data' => $artikel
            ]);        
        } 
        catch (\Exception $e) {
            return response()->json([
                'status' => $e->getMessage(),
                'error' => true,                
            ]);            
        }
    }
}
