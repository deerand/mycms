@role('Admin')
@if( $section->status_section === 0)
<button class="btn btn-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill" id="editSectionArtikelModal" data-id="{{$section->id}}" data-label="{{$section->nama}}">>
  <i class="la la-archive"></i>
</button>
<button class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" id="deleteSectionArtikelModal" data-id="{{$section->id}}" data-label="{{$section->nama}}">
  <i class="la la-trash-o"></i>
</button>
@else
<button class="btn btn-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill" id="editSectionArtikelModal" data-id="{{$section->id}}" data-label="{{$section->nama}}">>
  <i class="la la-archive"></i>
</button>
<button class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" id="deleteSectionArtikelModal" data-id="{{$section->id}}" data-label="{{$section->nama}}">
  <i class="la la-trash-o"></i>
</button>
@endif
@endrole