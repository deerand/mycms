<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use DB;
use App\SectionArtikelModel;
class SectionArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.section.index');
    }

    public function getSection()
    {
        $getSection = SectionArtikelModel::select(['id', 'nama','status_section']);

        return Datatables::of($getSection)
            ->addColumn('action',function($section){
                return view('admin.section.buttonRole',['section' => $section])->render();
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $section = new SectionArtikelModel();
            $section->users_id = Auth::user()->id;
            $section->nama = $request->input('nama');
            $section->status_section = 0;
            $section->save();

            return back()->with('success','Data Berhasil di Tambah');
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = SectionArtikelModel::findOrFail($id);
        return response()->json($section,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section = SectionArtikelModel::findOrFail($id);
        $section->nama = $request->input('nama');
        $section->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $section = SectionArtikelModel::findOrFail($id);
            $section->delete();
        } catch (\Exception $e) {
            return response()->json([
                'status' => $e->getMessage(),
                'error' => true,                
            ]);
        }

    }
}
