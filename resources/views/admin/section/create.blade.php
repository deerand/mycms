@extends('layouts.app')
@section('title')
  Tambah Section
@endsection
@section('headerPage')
  Tambah Section
@endsection
@section('quickAction')
    <ul class="m-nav">
        <li>
            <li class="m-nav__section m-nav__section--first">
                <span class="m-nav__section-text">Quick Actions</span>
            </li>
            <li class="m-nav__item">
                <a href="{{ route('sectionArtikel.index') }}" class="m-nav__link">
                    <i class="m-nav__link-icon flaticon-share"></i>
                    <span class="m-nav__link-text">Lihat Seluruh Section</span>
                </a>
            </li>
        </li>
    </ul>
@endsection

@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    <ul>
    @foreach ($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
    </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{route('sectionArtikel.store')}}">
    {{csrf_field()}}
    <div class="col-sm-12 m-form__group-sub">
        <label class="form-control-label">Nama</label>
        <input type="text" name="nama" class="form-control m-input" placeholder="Enter text">        
        <span class="m-form__help">Please enter your text</span>
    </div>
    <div class="col-xl-12 m--align-right">
        <button type="submit" class="btn btn-danger btn-sm m-btn--pill m-btn--air m-btn--wide">Submit</button>
        <button type="reset" class="btn btn-secondary btn-sm m-btn--wide m-btn--pill m--margin-left-5">Cancel</button>	
    </div>
</form>
@endsection
