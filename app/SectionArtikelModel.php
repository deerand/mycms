<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionArtikelModel extends Model
{
    protected $table = 'section_artikel';
    protected $fillable = [
        'users_id','nama','status_section'
    ];
}
