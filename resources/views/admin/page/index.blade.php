@extends('layouts.app')
@section('title')
    Lists All Page
@endsection
@section('headerPage')
    Lists All Page
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="la la-list"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Lists All Page
                </h3>
            </div>

        </div>
    </div>
    <div class="m-portlet__body no-pedding">
        <div class="list-section">                
            @forelse ($page as $item)
            
            <div class="list-section__item">
                <div class="section__content image-box">
                    <div class="section__desc">
                        <h5 class="section__title">{{ $item->judul }}</h5>
                        <div class="section__info">

                            <div class="section__info__item sm-text">
                                <span class="info__label">Posted By :</span>
                                <a href="" class="info__detail m-link">{{ $item->users->name }}</a>
                            </div>
                            <div class="section__info__item sm-text">
                                <span class="info__label">Language :</span>
                                <span class="info__detail">Indonesia</span>
                            </div>
                        </div>
                        <div class="section__status">
                            <span class="m-badge m-badge--danger m-badge--wide">Not Published yet</span>
                            <span class="m-badge m-badge--brand m-badge--wide">Draft</span>
                        </div>
                    </div>
                </div>
                <div class="section__action">
                    <div class="list__section__action">
                        <a href="#"
                            class="btn m-btn btn-primary btn-sm  m-btn--icon m-btn--square m-btn--air icon-only">
                            <span>
                                <i class="la la-list"></i>
                                <span>View List</span>
                            </span>
                        </a>
                        <a href="#" class="btn m-btn btn-success btn-sm m-btn--icon m-btn--air icon-only">
                            <span>
                                <i class="la la-pencil"></i>
                                <span>View List</span>
                            </span>
                        </a>
                        <a href="#" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air icon-only">
                            <span>
                                <i class="la la-eye"></i>
                                <span>View Page</span>
                            </span>
                        </a>
                        <a href="#"
                            class="btn m-btn btn-outline-danger btn-sm  m-btn--icon m-btn--pill icon-only m_sweetalert_5">
                            <span>
                                <i class="la la-trash"></i>
                                <span>Delete</span>
                            </span>
                        </a>
                    </div>
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--up m--hide-desktop"
                        m-dropdown-toggle="click" aria-expanded="true">
                        <a href="#"
                            class="btn m-btn btn-outline-metal m-btn--icon btn-sm m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                            <i class="la la-ellipsis-h"></i>
                        </a>
                        <div class="m-dropdown__wrapper" style="z-index: 101;">
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <!--Move Here-->
                                    </div>
                                </div>
                            </div>
                            <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <p>Kosong</p>
            @endforelse            
        </div>
    </div>
</div>
@endsection