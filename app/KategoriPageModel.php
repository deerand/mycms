<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPageModel extends Model
{
    protected $table = 'kategori_page';
    protected $fillable = [
        'users_id','nama','image'
    ];
    
    public function page()
    {
        return $this->hasMany('App\PageModel','kategori_id');
    }
    public function user() {
        return $this->belongsTo('App\User','id');
    }
}
